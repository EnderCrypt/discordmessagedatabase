package endercrypt.utility;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Utility
{
	public static final ExecutorService executor = Executors.newCachedThreadPool();

	// file
	private static boolean filenameCharacterSanitize(char c, char from, char to)
	{
		return (c >= from && c <= to);
	}

	public static String filenameSanitize(String text)
	{
		StringBuilder stringBuilder = new StringBuilder();

		for (char c : text.toCharArray())
		{
			if (filenameCharacterSanitize(c, '0', '9'))
			{
				stringBuilder.append(c);
				continue;
			}
			if (filenameCharacterSanitize(c, 'a', 'z'))
			{
				stringBuilder.append(c);
				continue;
			}
			if (filenameCharacterSanitize(c, 'A', 'Z'))
			{
				stringBuilder.append(c);
				continue;
			}
			if (c == ' ' || c == '-' || c == '.')
			{
				stringBuilder.append(c);
				continue;
			}
		}

		return stringBuilder.toString();
	}

	public static boolean createFolder(Path path)
	{
		try
		{
			if (Files.exists(path))
			{
				if (Files.isDirectory(path) == false)
				{
					return false;
				}
			}
			else
			{
				Files.createDirectories(path);
			}
			return true;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
	}

	public static boolean createParentFolder(Path path)
	{
		Path parent = path.getParent();
		if (parent != null)
		{
			return createFolder(parent);
		}
		return true;
	}

	public static Path randomFilename(Path directory, String extension)
	{
		String extensionString = extension.length() == 0 ? "" : "." + extension;
		while (true)
		{
			String uuid = UUID.randomUUID().toString();
			Path path = directory.resolve(uuid + extensionString);
			if (Files.exists(path) == false)
			{
				return path;
			}
		}
	}

	// resource
	public static InputStream getResourceStream(String path)
	{
		Objects.requireNonNull(path);
		return Utility.class.getResourceAsStream(path);
	}

	// math
	public static double round(double value, int decimals)
	{
		double multiplier = Math.pow(10, decimals);

		return Math.round(value * multiplier) / multiplier;
	}

	// string
	public static String capitalize(String text)
	{
		Objects.requireNonNull(text);
		if (text.length() == 0)
		{
			return text;
		}
		char first = text.charAt(0);
		char upper = Character.toUpperCase(first);

		String back_string = text.substring(1);

		return upper + back_string;
	}
}
