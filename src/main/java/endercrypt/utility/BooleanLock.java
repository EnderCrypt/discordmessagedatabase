package endercrypt.utility;

/**
 * a simple multi thread class which allows locking and unlocking
 * @author EnderCrypt
 */
public class BooleanLock
{
	private volatile boolean locked;

	private final Object lockObject = new Object();

	public BooleanLock()
	{
		this(false);
	}

	public BooleanLock(boolean locked)
	{
		this.locked = locked;
	}

	/**
	 * locks the lock, does nothing if its already locked
	 */
	public void lock()
	{
		synchronized (this.lockObject)
		{
			this.locked = true;
		}
	}

	/**
	 * unlocks the lock if its locked, otherwise does nothing
	 */
	public void unlock()
	{
		synchronized (this.lockObject)
		{
			if (this.locked)
			{
				this.locked = false;
				this.lockObject.notifyAll();
			}
		}
	}

	/**
	 * returns true or false depending on whether the lock is locked or not
	 * @return if locked
	 */
	public boolean isLocked()
	{
		return this.locked;
	}

	/**
	 * will block the current thread untill the lock gets unlocked (by another thread)
	 * @throws InterruptedException if the thread got interrupted
	 */
	public void waitForUnlocked() throws InterruptedException
	{
		synchronized (this.lockObject)
		{
			while (this.locked)
			{
				this.lockObject.wait();
			}
		}
	}

	/**
	 * behaves the same as waitForUnlocked() but the moment the lock gets unlocked, this method will re-lock it instantly
	 * @throws InterruptedException
	 * @see BooleanLock#waitForUnlocked
	 */
	public void waitForUnlockedThenLock() throws InterruptedException
	{
		synchronized (this.lockObject)
		{
			waitForUnlocked();
			lock();
		}
	}
}