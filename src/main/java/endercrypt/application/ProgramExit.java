package endercrypt.application;

@SuppressWarnings("serial")
public class ProgramExit extends Throwable
{
	private ExitCode exitCode;

	public ProgramExit(ExitCode exitCode)
	{
		this.exitCode = exitCode;
	}

	public ExitCode getExitCode()
	{
		return this.exitCode;
	}
}
