package endercrypt.application;

import java.io.File;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import endercrypt.Main;

public class DiscordMessageDatabase
{
	public static final File filename = new File(DiscordMessageDatabase.class.getProtectionDomain().getCodeSource().getLocation().getPath());

	public static final String name = DiscordMessageDatabase.class.getSimpleName();
	public static final String author = "EnderCrypt";
	public static final String version = "0.1";

	public static void main(String[] args)
	{
		// launch arguments
		Arguments arguments = new Arguments();
		CmdLineParser parser = new CmdLineParser(arguments);
		try
		{
			parser.parseArgument(args);
		}
		catch (CmdLineException e)
		{
			System.err.println("Error: " + e.getMessage());
			System.err.println("java -jar " + filename.getName() + " [Options]");
			parser.printUsage(System.err);
			ExitCode.BAD_CMD_ARGS.EndApplication();
		}

		// print version
		if (arguments.isVersion())
		{
			System.out.println(DiscordMessageDatabase.class.getSimpleName() + " by " + DiscordMessageDatabase.author + " Version " + DiscordMessageDatabase.version);
			ExitCode.SUCCESS.EndApplication();
		}

		// print help
		if (arguments.isHelp())
		{
			System.out.println("java -jar " + DiscordMessageDatabase.filename.getName() + " [Options] [Files ...]");
			parser.printUsage(System.out);
			System.out.println();
			System.out.println("Exit Codes: ");
			for (ExitCode exitCode : ExitCode.getExitCodes())
			{
				System.out.println("\t" + (exitCode.getCode() >= 0 ? " " : "") + exitCode.getCode() + " = " + exitCode.getDescription());
			}
			ExitCode.SUCCESS.EndApplication();
		}

		// launch
		try
		{
			Main.main(arguments);
		}
		catch (ProgramExit e)
		{
			e.getExitCode().EndApplication();
		}
		catch (InterruptedException e)
		{
			ExitCode.THREAD_INTERRUPTED.EndApplication();
		}
		catch (Exception e)
		{
			System.err.println("Fatal application exception!");
			e.printStackTrace();
			ExitCode.UNCAUGHT_EXCEPTION.EndApplication();
		}

		// finish
		ExitCode.SUCCESS.EndApplication();
	}
}
