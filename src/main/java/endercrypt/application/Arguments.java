package endercrypt.application;

import org.kohsuke.args4j.Option;

public class Arguments
{
	@Option(name = "-version", aliases = { "--version", "-v" }, required = false, usage = "prints the version number then exits")
	private boolean version = false;

	public boolean isVersion()
	{
		return this.version;
	}

	@Option(name = "-help", aliases = { "--help", "-h" }, required = false, usage = "prints the help information then exits")
	private boolean help = false;

	public boolean isHelp()
	{
		return this.help;
	}

	@Option(name = "-token", aliases = { "-t" }, required = true, usage = "discord login token")
	private String token;

	public String getToken()
	{
		return this.token;
	}

	@Option(name = "-skip-attachments", aliases = { "-skip-download", "-sa", "-sd" }, required = false, usage = "skips downloading attachments from files")
	private boolean skipAttachments = false;

	public boolean isSkipAttachments()
	{
		return this.skipAttachments;
	}

	@Option(name = "-user", aliases = { "-u" }, required = true, usage = "discord user id to download from")
	private long user;

	public long getUser()
	{
		return this.user;
	}

	@Option(name = "-first", aliases = { "-firstmessage", "-fm", "-f" }, required = false, usage = "the first message (required first time download a user)")
	private long first;

	public long getFirstMessage()
	{
		return this.first;
	}

	@Option(name = "-chunksize", aliases = { "-cs", "-size" }, required = false, usage = "the amount of messages to download per chunk")
	private int chunkSize = 100;

	public int getChunkSize()
	{
		return this.chunkSize;
	}

	@Option(name = "-chunkcount", aliases = { "-cc", "-count" }, required = true, usage = "the amount of message chunks to download")
	private int chunkCount;

	public int getChunkCount()
	{
		return this.chunkCount;
	}

	@Option(name = "-output", aliases = { "-o" }, required = false, usage = "output folder for user folder with database & attachments")
	private String output = "output";

	public String getOutput()
	{
		return this.output;
	}

	@Option(name = "-delay", aliases = { "-d" }, required = false, usage = "message chunk delay (seconds)")
	private long delay = 5;

	public long getDelay()
	{
		return this.delay;
	}
}
