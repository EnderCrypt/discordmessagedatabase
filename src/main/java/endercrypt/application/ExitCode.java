package endercrypt.application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import endercrypt.utility.Utility;

public class ExitCode implements Comparable<ExitCode>
{
	private static List<ExitCode> exitCodes = new ArrayList<>();

	public static final ExitCode SUCCESS = new ExitCode("The command executed successfully");

	public static final ExitCode UNIX_GENERAL_ERROR = new ExitCode("(UNIX) Catchall for general errors");
	public static final ExitCode UNIX_MISUSE_SHELL = new ExitCode("(UNIX) Misuse of shell builtins");

	public static final ExitCode BAD_CMD_ARGS = new ExitCode("Bad command-line arguments");
	public static final ExitCode UNCAUGHT_EXCEPTION = new ExitCode("Uncaught exception");
	public static final ExitCode THREAD_INTERRUPTED = new ExitCode("Application thread was unexpectedly interrupted");

	public static final ExitCode FIRST_ID_REQUIRED = new ExitCode("id of starting message required for starting collection");
	public static final ExitCode DATABASE_PATH_OPCCUPIED = new ExitCode("the output folder is already occupied");
	public static final ExitCode CHUNK_SIZE_INVALID = new ExitCode("invalid chunk size (expected: 1 to 100)");

	static
	{
		Collections.sort(exitCodes);
	}

	private static int failure_code_counter = 0;

	public static List<ExitCode> getExitCodes()
	{
		return Collections.unmodifiableList(exitCodes);
	}

	private int code;
	private String description;

	private ExitCode(String description)
	{
		this(failure_code_counter++, description);
	}

	private ExitCode(int code, String description)
	{
		this.code = code;
		this.description = Utility.capitalize(description);

		exitCodes.add(this);
	}

	public int getCode()
	{
		return this.code;
	}

	public String getDescription()
	{
		return this.description;
	}

	@Override
	public int compareTo(ExitCode other)
	{
		return Integer.compare(other.getCode(), this.getCode());
	}

	public void EndApplication()
	{
		System.out.println("Exit: ( " + getCode() + " ) " + getDescription());
		System.exit(getCode());
	}
}