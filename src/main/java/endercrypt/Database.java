package endercrypt;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import endercrypt.application.ExitCode;
import endercrypt.application.ProgramExit;
import endercrypt.utility.Utility;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Message.Attachment;

public class Database
{
	private static final Path scriptDirectory = Paths.get("scripts");

	private final Path databasePath;
	private final Connection connection;

	public Database(Path databasePath) throws SQLException, ProgramExit, IOException
	{
		// path
		this.databasePath = databasePath;
		if (Utility.createParentFolder(this.databasePath) == false)
		{
			throw new ProgramExit(ExitCode.DATABASE_PATH_OPCCUPIED);
		}

		// connection
		this.connection = DriverManager.getConnection("jdbc:sqlite:" + databasePath);

		// create
		executePrepareStatement("CreateMessagesTable.sql");
		executePrepareStatement("CreateAttachmentsTable.sql");
	}

	public Connection connection()
	{
		return this.connection;
	}

	private static String scriptMethodNameExtraction()
	{
		final String prefix = "script";
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		String methodName = stackTrace[2].getMethodName();
		if (methodName.startsWith(prefix) == false)
		{
			throw new IllegalArgumentException("method " + methodName + " needs to start with \"" + prefix + "\" in its name");
		}
		return methodName.substring(prefix.length()) + ".sql";
	}

	private static String loadScript(String script) throws SQLException, IOException
	{
		Path scriptPath = scriptDirectory.resolve(script);
		if (Files.exists(scriptPath) == false || Files.isRegularFile(scriptPath) == false)
		{
			throw new FileNotFoundException("script file " + scriptPath + " not found");
		}
		return new String(Files.readAllBytes(scriptPath));
	}

	public PreparedStatement prepareStatement(String script) throws SQLException, IOException
	{
		return connection().prepareStatement(loadScript(script));
	}

	public void executePrepareStatement(String script) throws SQLException, IOException
	{
		try (PreparedStatement statement = prepareStatement(script))
		{
			statement.execute();
		}
	}

	// SCRIPTS //

	public synchronized long scriptMessagesGetLatestMessageID() throws SQLException, IOException
	{
		try (PreparedStatement statement = prepareStatement(scriptMethodNameExtraction()))
		{
			statement.execute();
			try (ResultSet resultSet = statement.getResultSet())
			{
				if (resultSet.next())
				{
					return resultSet.getLong("MessageID");
				}
			}
		}
		return -1L;
	}

	public synchronized void scriptMessagesInsert(Message message) throws SQLException, IOException
	{
		scriptMessagesInsert(message.getIdLong(), message.getAuthor().getIdLong(), message.getContentRaw(), message.getTimeCreated().toEpochSecond());
	}

	public synchronized void scriptMessagesInsert(long messageId, long author, String message, long creationDate) throws SQLException, IOException
	{
		try (PreparedStatement statement = prepareStatement(scriptMethodNameExtraction()))
		{
			int index = 1;
			statement.setLong(index++, messageId);
			statement.setLong(index++, author);
			statement.setLong(index++, creationDate);
			statement.setString(index++, message);

			statement.executeUpdate();
		}
	}

	public synchronized void scriptAttachmentsInsert(Message message, Attachment attachment, String localFilename) throws SQLException, IOException
	{
		if (message.getAttachments().contains(attachment) == false)
		{
			throw new IllegalArgumentException("message " + message + " is not attached to " + attachment);
		}
		scriptAttachmentsInsert(message.getIdLong(), attachment.getIdLong(), attachment.getFileName(), attachment.getSize(), attachment.getUrl(), localFilename);
	}

	public synchronized void scriptAttachmentsInsert(long messageId, long attachmentId, String filename, int size, String url, String localFilename) throws SQLException, IOException
	{
		try (PreparedStatement statement = prepareStatement(scriptMethodNameExtraction()))
		{
			int index = 1;
			statement.setLong(index++, messageId);
			statement.setLong(index++, attachmentId);
			statement.setString(index++, filename);
			statement.setInt(index++, size);
			statement.setString(index++, url);
			if (localFilename == null)
				statement.setNull(index++, Types.VARCHAR);
			else
				statement.setString(index++, localFilename);

			statement.executeUpdate();
		}
	}
}
