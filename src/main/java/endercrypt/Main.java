package endercrypt;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import javax.security.auth.login.LoginException;

import org.apache.commons.io.FilenameUtils;

import endercrypt.application.Arguments;
import endercrypt.application.ExitCode;
import endercrypt.application.ProgramExit;
import endercrypt.utility.BooleanLock;
import endercrypt.utility.Utility;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.Message.Attachment;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;

public class Main
{
	public static void main(Arguments arguments) throws ProgramExit, InterruptedException, LoginException, SQLException, IOException
	{
		// discord
		JDABuilder jdaBuilder = new JDABuilder(AccountType.CLIENT);
		jdaBuilder.setToken(arguments.getToken());
		JDA jda = jdaBuilder.build();
		jda.awaitReady();

		User user = jda.getUserById(arguments.getUser());
		if (user == null)
		{
			user = jda.getUserCache().getElementById(arguments.getUser());
			if (user == null)
			{
				System.out.println("Waiting for user " + arguments.getUser() + " to appear (get them to DM you)...");

				AtomicReference<User> userReference = new AtomicReference<>();
				BooleanLock lock = new BooleanLock(true);
				lock.lock();

				EventListener eventListener = new ListenerAdapter()
				{
					@Override
					public void onPrivateMessageReceived(PrivateMessageReceivedEvent event)
					{
						User author = event.getAuthor();
						if (author.getIdLong() == arguments.getUser())
						{
							userReference.set(author);
							lock.unlock();
						}
					}
				};

				jda.addEventListener(eventListener);

				lock.waitForUnlocked();
				user = userReference.get();

				jda.removeEventListener(eventListener);
			}
		}

		// arg check
		if (arguments.getChunkSize() < 1 || arguments.getChunkSize() > 100)

		{
			throw new ProgramExit(ExitCode.CHUNK_SIZE_INVALID);
		}

		// vars
		System.out.println("User: " + user.getAsTag());
		PrivateChannel privateChannel = user.openPrivateChannel().complete();
		Path outputFolder = Paths.get(arguments.getOutput());
		Path userFolder = outputFolder.resolve(String.valueOf(user.getIdLong()));
		Path userAttachmentFolder = userFolder.resolve("attachments");
		Path uesrDatabasePath = userFolder.resolve("database.db");
		System.out.println("Database: " + uesrDatabasePath);
		Database database = new Database(uesrDatabasePath);

		// first message //
		long latestMessageID = database.scriptMessagesGetLatestMessageID();
		if (latestMessageID > 0)
		{
			System.out.println("Continuing from message: " + latestMessageID);
		}
		else
		{
			latestMessageID = arguments.getFirstMessage();
			if (latestMessageID == 0)
			{
				throw new ProgramExit(ExitCode.FIRST_ID_REQUIRED);
			}

			System.out.println("Retriving first message from discord...");
			Message message = privateChannel.getHistoryAround(latestMessageID, 1).complete().getMessageById(latestMessageID);
			database.scriptMessagesInsert(message);
			Thread.sleep(TimeUnit.SECONDS.toMillis(arguments.getDelay()));
		}

		// chunks //
		boolean reachedEnd = false;
		for (int i = 0; i < arguments.getChunkCount(); i++)
		{
			System.out.println("Processing chunk " + (i + 1) + "/" + arguments.getChunkCount() + " ...");

			// load history
			List<Message> messageHistory = new ArrayList<>(privateChannel.getHistoryAfter(latestMessageID, arguments.getChunkSize()).complete().getRetrievedHistory());
			Collections.reverse(messageHistory);
			if (messageHistory.isEmpty())
			{
				System.out.println("-{ Chunk is empty }-");
				reachedEnd = true;
				break;
			}
			for (Message message : messageHistory)
			{
				database.scriptMessagesInsert(message);
				for (Attachment attachment : message.getAttachments())
				{
					String attachmentInformation = attachment.getId() + " (" + attachment.getFileName() + " " + attachment.getSize() + " bytes)";
					String localFilename = null;
					if (arguments.isSkipAttachments())
					{
						System.out.println("skipping attachment " + attachmentInformation);
					}
					else
					{
						System.out.println("Writting attachment " + attachmentInformation);
						String extension = FilenameUtils.getExtension(attachment.getFileName());
						Utility.createFolder(userAttachmentFolder);
						Path attachmentPath = Utility.randomFilename(userAttachmentFolder, extension);
						Files.createFile(attachmentPath);
						attachment.downloadToFile(attachmentPath.toFile());
						localFilename = attachmentPath.getFileName().toString();
					}
					database.scriptAttachmentsInsert(message, attachment, localFilename);
				}
			}
			latestMessageID = messageHistory.get(messageHistory.size() - 1).getIdLong();
			System.out.println("Wrote " + messageHistory.size() + " messages to database");

			// check if end
			if (messageHistory.size() < arguments.getChunkSize())
			{
				System.out.println("-{ Chunk percieved as end }-");
				reachedEnd = true;
				break;
			}

			// sleep
			Thread.sleep(TimeUnit.SECONDS.toMillis(arguments.getDelay()));
		}

		if (reachedEnd == false)
		{
			System.out.println("Note: There's problably more messages available");
		}

		// done //
		System.out.println("Done!");

		jda.shutdown();
	}
}
